\section{Study Design}\label{design}
This section describes the setup of our quasi-experiment that aims to identify fault-prone states and changes in clone genealogies. Figure~\ref{fig:process} shows an overview of the steps we use to extract clone information from a source code repository and build clone genealogies. We describe our steps in more detail in the following subsections. We share our analytic scripts and data at: \url{https://github.com/swatlab/clone_genealogies}.
\begin{figure*}[h!]
\centering
    %\begin{center}
        \includegraphics[width=\columnwidth]{../figs/flow_graph.pdf}
    %\end{center}
    %\vspace{-30pt}
    \caption{Overview of the Analysis Process.}
    \label{fig:process}
    %\vspace{-15pt}
\end{figure*}
\subsection{Subject Systems}
We select four open-source Java systems as the subjects systems. All of the subject systems possess a long development history, which is suitable for our clone genealogy study.

\begin{itemize*}
  \item {\sc Apache Ant} is an open-source build-tool with an extensive Java library. We study its revision history from January 2000 to July 2016.
  \item {\sc ArgoUML} is a UML-modelling software system. We study its commit history from January 1998 to January 2015 (\ie{} until the most recent version of the project).
  \item {\sc JEdit} is an open-source text editor built for programmers. It is written in Java, and provides support for editing more than 200 programming languages. Many plug-ins have been written for {\sc JEdit}. In this study, we only examine the editor. The project started in 1998 and is still under development. We examine its revision history from September 2001 to July 2015.
  \item \textsc{Maven} is a build automation tool used primarily for Java projects. We study its commit history from September 2003 to July 2016.
\end{itemize*}

Table \ref{tab:datadescription} summarizes the characteristics of each system. We use the SLOCCount tool~\cite{sloccount} to count the total number of lines of code (LOC) and the percentage of Java code in each project. For each project, we provide LOC for the last studied revision. Table~\ref{tab:buggy_cnt} shows the numbers of faulty changes and the numbers of clean changes for each subject system.

\BLUE{
We examined the length of the clone genealogies contained in the selected software systems and observed that more than 50\% of the genealogies only experienced 1-2 changes.
%
%and the number of genealogies starting from a specific commit, we performed the following descriptive analyses.
}
Figures  \ref{fig:nicad_dist} and \ref{fig:iclones_dist} show the frequency of the number changes in each of the clone genealogies. Overall, although the studied systems contain high numbers of genealogies, the genealogies tend to be short.
\BLUE{
In this paper, we do not consider the unchanged clone pattern ($UNC_p$), hence, all the studied clone genealogies experienced at least one change.}
%We observe that  %, \ie{} the length of the genealogies are short in general.
%Hence, although the studied systems have high numbers of genealogies, the genealogies tend to be short.  %obwhy in some studied systems, such ArgoUML, there are a high number of clone genealogies.
Figure \ref{fig:starting_cnt} depicts the number of clone genealogies deriving from a specific commit. In this figure, we eliminated outliers. The median value for each project is less than 3; implying that there are only few clone genealogies starting from each commit.

\begin{table}[t]
%\vspace{-22pt}
\caption{Characteristics of the Systems}
\centering
\begin{tabular}{lrrrrrrrrr}
  \toprule
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  \multirow{2}{*}{System} & \multirow{2}{*}{\# LOC} & \% Java & \multirow{2}{*}{\# Commits} & \multicolumn{2}{c}{\# Clones} &\multicolumn{2}{c}{\# Genealogies}  \\ \cline{5-8}
  & & code & & NiCad & iClones & NiCad & iClones\\ \midrule
  {\sc ArgoUML} & 253.1k & 70.1 & 17.8k & 96.7M & 17.3M & 16.6k & \BLUE{7.6k}  \\\hline
  {\sc Ant} & 172.3k & 80.0 & 13.4k & 7.8M & 7.2M & 5.5k & \BLUE{7.1k} \\\hline
  {\sc JEdit} & 215.8k & 55.9 & 7.7k & 3.7M & 8.8M & 6.8k & \BLUE{6.5k} \\ \hline
  \textsc{Maven} & 88.2k & 79.8 & 10.3k & 1.6M & 3.3M & 0.7k & \BLUE{0.4k} \\ \bottomrule
\end{tabular}\label{tab:datadescription}
%\vspace{-16pt}
\end{table}


\begin{table}[t]
%\vspace{-12pt}
\caption{Number of faulty and clean clone changes in each system.}
\centering
\resizebox{\linewidth}{!}{
    \begin{tabular}{lrrrr|rrrr}
    \toprule
               & \multicolumn{4}{c|}{NiCad} & \multicolumn{4}{c}{iClones}  \\
    \cline{2-9}
               & ArgoUML & Ant & JEdit & Maven & ArgoUML & Ant & JEdit & Maven \\
    \midrule
           Faulty & 3,246 & 643 & 49 & 300 & 2,967 & 363 & 48 & 256 \\
    \hline
           Clean & 2,629 & 3,440 & 319 & 436 & 1,876 & 2,292 & 273 & 1,168\\
    \bottomrule
      \end{tabular}
    \label{tab:buggy_cnt}
}
%\vspace{-16pt}
\end{table}

\begin{figure}[t]
    \centering
    \subfigure[ArgoUML]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_nicad/Rplot002}
    }
    \hfill
    \subfigure[Ant]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_nicad/Rplot001}
    }
    \hfill
    \subfigure[JEdit]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_nicad/Rplot003}
    }
    \hfill
	\subfigure[Maven]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_nicad/Rplot004}
    }
    \caption{Percentage of the frequency of the number of changes in a studied clone genealogy detected by NiCad}
    \label{fig:nicad_dist}
\end{figure}


\begin{figure}[!]
    \centering
    \subfigure[ArgoUML]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_iclones/Rplot002}
    }
    \hfill
    \subfigure[Ant]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_iclones/Rplot001}
    }
    \hfill
    \subfigure[JEdit]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_iclones/Rplot003}
    }
    \hfill
	\subfigure[Maven]{
        \includegraphics[width = 0.47\textwidth]{../figs/gen_len_iclones/Rplot004}
    }
    \caption{Percentage of the frequency of the number of changes in a studied clone genealogy detected by iClones}
    \label{fig:iclones_dist}
\end{figure}


\begin{figure}[t]
	\hspace*{\fill}
    \subfigure[NiCad]{
        \includegraphics[width = 0.35\textwidth]{../figs/nicad_gene_starting}
        \label{fig:start_cnt_nicad}
    }
    \hspace*{\fill}
    \subfigure[iClones]{
        \includegraphics[width = 0.35\textwidth]{../figs/iclones_gene_starting}
        \label{fig:start_cnt_iclones}
    }
    \hspace*{\fill}
    \caption{Number of clone genealogies starting from a specific commit}
    \label{fig:starting_cnt}
\end{figure}




%\subsection{Analysis and Modeling Techniques}\label{sec:method}
%We examine the first two research questions using the odds ratio (OR) and validate the statistical significance of our results using the Chi-Square Test. The Chi-Square test \cite{sheskin} determines if there are non-random associations between two categorical variables (\eg{} a clone evolutionary pattern and the occurrence of faults). The odds ratio compares the odds of an event occurring in two different groups, the ``control" group and the ``experimental" group. An $OR=1$ implies that the event is equally likely in both the control and experimental group, an $OR>1$ implies that the event is more likely in the experimental group, and an $OR<1$ implies that it is more likely in the control group.

%\FOUTSE{To be updated!}
%For the last research question, we compute product, process, and clone genealogy metrics. We also compute the number of faults reported after a clone pair experiences an evolutionary pattern. From the measurements obtained, we create linear regression models that set the number of reported faults into relation with our three groups of metrics. The linear regression models have the following form:
%\begin{eqnarray}
%Faults &= &\sum_i\alpha_iProductM_i + \sum_j\beta_jProcessM_j \nonumber\\
% &+& \sum_k\gamma_kGenealogyM_k + \delta
%\end{eqnarray}
%
%With this model, we investigate the statistical relationships between product, process and genealogy metrics, which are represented by the regression variables ($ProductM_i$,\\ $ProcessM_j$, and $GenealogyM_k$), and the number of reported faults, represented by the dependent variable of the model ($Faults$). We follow the same methodology as in the work of Cataldo \emph{et al.} \cite{Cataldo09-SoftwareDependencies}. First, we perform a correlation analysis to consider possible inter-relations between our measurements. Next, we construct several logistic regression models to investigate the relative impact of each of our three groups of metrics on future faults. We create the models following a hierarchical modelling approach: we start out with a baseline model that contains only product metrics as regression variables.
%We then build subsequent models by adding step by step, respectively, process metrics and clone genealogy metrics. We report for each statistical model, the explanatory power, $\chi^2$, of the model and the percentage of deviance explained. The deviance of a model $M$ is defined as $D(M)=-2.LL(M)$, where $LL(M)$ is the log-likelihood of the model $M$. The deviance explained is the ratio between $D(Faults\thicksim Intercept)$ and $D(M)$.
%For each subsequent model $M$, we also test the statistical significance of the difference between $M$ and the model from which $M$ is derived. We report the corresponding $p$-values. We chose to follow a hierarchical modelling approach because contrary to a step-wise modelling approach, the hierarchical approach has the advantage of minimizing the artificial inflation of errors and therefore the overfitting \cite{Cataldo09-SoftwareDependencies}.
%we build prediction models using the Random Forest algorithm in Weka\footnote{\url{http://www.cs.waikato.ac.nz/ml/weka/}}. A Random Forest \cite{Breiman01-RF} classifier consists of a set of tree-structured classifiers. We choose Random Forest for this work because it is one of the most successful classification methods, with performance on the level of other machine learning techniques, such as boosting and support vector machines. It is fast, robust to noise, and does not overfit \cite{robnik04-IRF}.
%
%We use the permutation accuracy importance measure implemented in the R\footnote{\url{http://http://cran.r-project.org/}} add-on package \emph{party} \cite{Hothorn06-party} to assess the importance of each predictor in our models. The permutation accuracy importance measure is the most advanced variable importance measure available in Random Forests \cite{Breiman01-RF}.
%For a variable $X_j$ (representing a clone pair metric), the importance is measure by the difference in prediction accuracy before and after permuting $X_j$. The rationale of this importance measure being that: when a predictor variable $X_j$ is randomly permutated, its original association with the response $Y$ (\eg{} the fault-proneness of the clone pair) is broken. Therefore, the use of the permuted variable $X_j$, together with the remaining unpermuted predictor variables to predict $Y$ will result in a substantial decrease of the prediction accuracy if the original variable $X_j$ was associated with $Y$.
%Using the cforest implementation in R \footnote{\url{http://http://cran.r-project.org/}}, we assess the importance of each predictor in the model.
%widely used to evaluate prediction models \cite{Meneely-08-PFD,Moser08-ChangevsStatic},
%To evaluate the performances of our prediction models, we use a $10$-fold cross validation approach and compute the following Information Retrieval (IR) metrics \cite{Baeza-Yates:1999}:
%\begin{itemize}
%\item \textbf{Precision:} the number of clone pairs correctly classified as faulty over the number of all clone pairs classified as faulty.
%\item \textbf{Recall:} the number of clone pairs correctly classified as faulty over the total number of faulty clone pairs.
%\item \textbf{F-measure:} the harmonic mean of precision and recall.
%\end{itemize}
%This technique is widely used to evaluate prediction models \cite{Meneely-08-PFD,Moser08-ChangevsStatic}.

\subsection{Data Preprocessing}
To analyze a repository's history, Git provides high-performance functions to extract changed files, renamed files, and blame faulty files.
Since the source code of ArgoUML and JEdit is managed by SVN, we use Git's \texttt{git-svn} command to convert the two systems' repositories to Git. Then, we use the following command to extract each commit's commit ID, committer email, commit date, and commit message:\\

\texttt{git log -{}-pretty=format:"\%H,\%ae,\%ai,\%s"}


\subsection{Detecting Faulty Changes}\label{szz}
We leverage the SZZ algorithm~\cite{sliwerski2005changes} to detect changes that introduced faults. We first apply Fischer et al.'s heuristic~\cite{fischer2003populating} to identity fault-fixing commits by using regular expressions to detect bug IDs from the studied commit messages. We then mine the subject systems' bug tracking systems (\emph{issuezilla} for ArgoUML, \emph{Jira} for Ant and Maven, and \emph{SourceForge} for JEdit) to extract their bugs' creation date. Next, we extract the modified files of each fault-fixing commit through the following Git command:\\

\texttt{git log [commit-id] -n 1 {-{}-}name-status}\\

\noindent
In this paper, we only take modified Java files into account. Given each file $F$ in a commit $C$, we extract $C$'s parent commit $C'$. For Ant and Maven, we use the \texttt{[commit-id]\^} command to obtain $C'$; while for ArgoUML and JEdit, since their repositories were converted from SVN, we find the $C$'s precedent commit $C'$ by time, \ie{} $C'$ is the nearest commit prior to $C$. Then, we use Git's \texttt{diff} command to extract $F$'s deleted lines. We apply Git's \texttt{blame} command to identify commits that introduced these deleted lines, noted as the ``candidate faulty changes''. We eliminate the commits that only changed blank and comment lines. Finally, we filter the commits that were submitted after their corresponding bugs' creation date.

\subsection{Extracting Clone Genealogies}
Extracting clone genealogies from each subject system requires three steps: removing test files, detecting clones, and building clone genealogies.

\textbf{Removing Test Files.} Test files are frequently copied and then modified to create multiple test cases, so they often contain clones. These files are used for development purposes and not used during the normal execution of the system. They may also contain syntactically incorrect code. For all these reasons, we believe that clones in test code should be studied separately from clones in production code. Therefore, we exclude test files from our study. In future work, we plan to examine the evolution of clones in test code which are nevertheless clones and need to be maintained. To remove the test files, we perform a search on each system for files and folders with a filename containing the word ``test''. We then manually verify each file before removing it from the study to prevent the automatic removal of a non-test file, such as a file with the name ``updateState.java''. \BLUE{At the end of this semi-automatic process, we also manually verify all the remaining files in our data set, to ensure that no semantically test-related files remain in the data set of our study.}

\textbf{Detecting Clones.}
We use two existing clone detection tools to detect clones in the four systems: {\sc NiCad}\cite{Roy08-NiCad} and {\sc iClones}~\cite{gode2009incremental}. We use the most recent versions of both tools: {\sc NiCad-4} and {\sc iClones-0.2}. We select these two clone detection tools because they are recommended by Svajlenko et al.~\cite{svajlenko2014evaluating} who compared the performance of 11 clone detection tools from the literature. Today, NiCad and iClones are considered as state-of-the-art tools by the clone community~\cite{svajlenko2014evaluating}.

Both NiCad and iClones use a hybrid approach to detect clones. We use the default settings of Nicad to detect clones greater than 10 lines of code, while using the default setting of iClons to detect clones with minimum 100 tokens. We detect identical clones and clones where the variable names are different (\ie{} ``blindrename''). The same settings were used by Svajlenko et al.~\cite{svajlenko2014evaluating} in their comparison of 11 clone detection tools. With these settings, NiCad and iClones were found to achieve higher precision and recall in comparison to the other 9 clone detection tools that were studied. %They ensure that the tools achieve their best accuracy \Foutse{precision/recall? please double check!}.

We use the Git \texttt{checkout} command to retrieve a system's snapshot for a specific commit. Then we perform clone detection on each of the snapshots of the studied systems. Table \ref{tab:datadescription} summarizes the number of clone pairs and clone genealogies found in each subject system using both clone detection tools. For ArgoUML and Ant, Nicad detected more clone pairs than iClones; while for JEdit and Maven, iClones detected more clone pairs. This difference in the number of clones found by the two detection tools is likely due to the lack of agreement on the definition of code clones~\cite{Lakhotia03} and to the implementation of the tools.


%\ANLE{For ArgoUML, NiCad's genealogies are much more than iClones'. I repeated the experiment, but got the same result.}
%In an examination of the {\sc CCFinder} output, we find the results to have a high recall but low precision. This may be explained by {\sc CCFinder}'s pre-processing step that tokenizes the input files and normalizes all the literals and identifiers in the code. We manually inspect the {\sc CCFinder} output, and note a high number of false positive clones caused by the normalization process. Before building the genealogies, we manually filter the {\sc CCFinder} results to remove some of the methods that contained a high number of false positive clones.

%{\sc CCFinder} detects a large number of clones in {\sc ArgoUML}. Due to hardware limitations when generating clone genealogies, we limit our study of {\sc ArgoUML} using {\sc CCFinder} to the period between January 1998 and October 2002, which coincides with release 0.12 (2576 commits).

\textbf{Building Clone Genealogies.}
Each clone detection tool outputs a list of clones within each source code repository. To create a set of clone pair genealogies, we link the clone pairs between each commit. A change to a clone can affect its size. A change to the file containing the clone, even if it does not affect the clone itself, can shift the clone's line numbers. To account for these changes when mapping clones, we use the Git \emph{diff} command to query for a list of changes to each Java file. We limit our genealogies to describe only changes that modify the clone contents, not the clone line numbers. This is because a shift in the line numbers cannot cause the clone pair to transition to a different state.

We build a clone genealogy for each clone pair detected by the clone detection tool. We first extract a system's commit sequence list. For ArgoUML and JEdit, which were originally managed by SVN, we sort their commits by time in ascending order. For Ant and Maven, which are managed by Git, we make a list and put a system's last commit as the first element. Then we recursively look for the list's last element's parent commit until the system's first commit is met. We reverse the lists to obtain Ant and Maven's commit sequence lists.

For each clone pair, we track its modification in every commit along the commit sequence list. If a commit, $C_{new}$, changed a file that contains code in the clone pair, we use the \texttt{diff} command to compare the commit with its previous commit, $C_{old}$, in order to check whether the clone snippets are modified and to map the start line and end line numbers from $C_{old}$ to $C_{new}$. We use Python's third-party patch parsing library \emph{whatthepatch}~\cite{whachthepatch} to extract the line mapping on a clone file between $C_{old}$ and $C_{new}$. In case that the first lines, $L_1 \sim L_n$, of a clone snippets are deleted in $C_{old}$ and no corresponding line added in $C_{new}$ to replace these deleted lines, we map $L_{n+1}$ from $C_{old}$ to $C_{new}$ as the start line. Similarly, in case that the last lines, $L_{x} \sim L_{x+n}$ are deleted in $C_{old}$ and no corresponding line added in $C_{new}$ to replace them, we map $L_{x-1}$ from $C_{old}$ to $C_{new}$ as the end line.

We decide whether a clone is changed when there is any deleted or added lines performed in the clone's boundaries.
If a clone is modified, we determine whether the new state of the clone pair is inconsistent ($I_s$) or consistent ($C_s$). We verify this by searching the clone pair list generated by a clone detection tool. We query the list for a matching clone pair in the new commit, $C_{new}$, that contains the start and end line numbers of the clone pair. If no clone pair is found, then the state of the clone is inconsistent and an inconsistent state ($I_s$) is added to the genealogy. If a clone is found, then a consistent state ($C_s$) is added to the genealogy. This process is repeated for each commit in the commit sequence list or until one or both of the clones is deleted. We use the following command to extract renamed files in a new commit:\\

\texttt{git diff [old-commit] [new-commit] -{}-name-status -M}\\

\noindent
This command can extract file pair, where a file is deleted and another file is added in the new commit and the two files have a code similarity greater than 50\%. In this paper, we only consider the file pairs with more than 99\% of code similarity as renamed files.
When searching the clone sequence list, we allow a matching clone to be bigger than the clone pair, and contain the smaller clone. For example, if one of the clones in a clone pair is from lines 1 to 10, a matching clone in the clone pair list could be from lines 1 to 20. Although we add the bigger clone from the clone pair list to our genealogy, we continue to monitor only the smaller clone to generate the genealogy. The bigger clone (\ie{} lines 1 to 20 in our example) might disappear in a future revision, but the smaller clone (\ie{} lines 1 to 10 in our example) persists after the bigger clone is removed. %\RED{Furthermore, a genealogy is generated for the larger clone pair, so its genealogy is also considered when training and testing our prediction models.} \ANLE{do we really train and test the models, I rather think we just build/fit linear models and read results.}

